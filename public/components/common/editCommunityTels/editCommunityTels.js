(function (vc, vm) {
    vc.extends({
        data: {
            communityId:"",
            communityTels:[],
            editCommunityTelInfo: {
                id: '',
                communityId: '',
                tel: ''
            },
            addCommunityTelModel: false
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('editCommunityTels', 'editCommunityTelsModel',
                function (_params) {
                    $('#editCommunityTelsModel').modal('show');
                    vc.component.communityId = _params;
                    vc.component.listCommunityTels()
                });
        },
        methods: {
            listCommunityTels: function () {
                let _param = {
                    params: {
                        communityId: vc.component.communityId
                    }
                };
                vc.http.apiGet('/communityTel.listCommunityTel', _param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        vc.component.communityTels = _json.data;
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            },
            editCommunityTelValidate: function () {
                return vc.validate.validate({
                    editCommunityTelInfo: vc.component.editCommunityTelInfo
                }, {
                    'editCommunityTelInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系方式不能为空"
                        }
                    ]
                });
            },
            openAddCommunityTelModel: function () {
                vc.component.addCommunityTelModel = true
            },
            closeAddCommunityTelModel: function () {
                vc.component.editCommunityTelInfo = {
                    id: '',
                    communityId: '',
                    tel: ''
                }
                vc.component.addCommunityTelModel = false
            },
            deleteCommunityTel: function (communtityTel) {
                let _param = {
                    params: {
                        id: communtityTel.id
                    }
                };
                vc.http.apiGet('/communityTel.deleteCommunityTel', _param,
                    function (json, res) {
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            vc.component.listCommunityTels();
                            return;
                        } else {
                            vc.toast(_json.msg);
                        }
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            },
            saveCommunityTelInfo: function () {
                if (!vc.component.editCommunityTelValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if(vc.component.editCommunityTelInfo.communityId == ''){
                    vc.component.editCommunityTelInfo.communityId = vc.component.communityId
                }
                vc.http.apiPost('/communityTel.saveCommunityTel', JSON.stringify(vc.component.editCommunityTelInfo), {
                    emulateJSON: true
                },
                    function (json, res) {
                        vc.component.listCommunityTels();
                        vc.component.closeAddCommunityTelModel();
                        vc.toast("保存成功");
                        return;
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            }
        }
    });
})(window.vc, window.vc.component);