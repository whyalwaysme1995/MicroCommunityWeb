(function (vc) {
    vc.extends({
        data: {
            addCommunityTelInfo: {
                tel: '',
                communityId: ''
            }
        },
        _initMethod: function () {
            vc.on('addCommunityTel', "openaddCommunityTelModal", function (_data) {
                $that.clearAddCommunityTelInfo();
                $that.addCommunityTelInfo.communityId = _data;
                $('#addCommunityTelModel').modal('show');
            });
        },
        methods: {
            addCommunityTelValidate() {
                return vc.validate.validate({
                    addCommunityTelInfo: $that.addCommunityTelInfo
                }, {
                    'addCommunityTelInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系电话不能为空"
                        }
                    ]
                });
            },
            saveCommunitySettingInfo: function () {
                $that.addCommunityTelInfo.tel = $that.addCommunityTelInfo.tel.trim();

                if (!$that.addCommunityTelValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.http.apiPost(
                    '/communityTel/saveCommunityTel',
                    JSON.stringify($that.addCommunityTelInfo), {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addCommunityTelModel').modal('hide');
                            $that.clearaddCommunityTelInfo();
                            vc.emit('communitySettingManage', 'listCommunitySetting', {});
                            vc.message(_json.msg);
                            return;
                        }
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(errInfo);
                    });
            },
            clearaddCommunityTelInfo: function () {
                $that.addCommunityTelInfo = {
                    tel: '',
                    communityId: ''
                };
            }
        }
    });
})(window.vc);